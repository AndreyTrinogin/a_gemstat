/*****************************************************
* Test the expression model
* Input: sequence, expression, motif, factor expr, cooperativity rules,
*   activator list, repression rules
* Output: expression of training data
* File formats:
* (1) Sequence: Fasta format
* (2) Expression: one line per sequence
*       <seq_name expr_1 ... expr_m>
* (3) Motif: Stubb format
* (4) Factor expression: one line per factor
*       <factor expr_1 ... expr_m>
* (5) Cooperativity: list of cooperative factor pairs
* (6) Factor roles: the role field is either 1 (yes) or 0 (no)
*       <factor activator_role repressor_role>
* (7) Repression: list of <R A> where R represses A
* (8) Parameters: the format is:
*       <factor binding activation repression>
*       <factor1 factor2 coop>
*       <basal_transcription = x>
*     where repression is optional, and the coop. lines are optional.
* Note that (5), (6), (7) and (8) may be empty
******************************************************/
#include "ExprPredictor.h"

int main( int argc, char* argv[] )
{
    // command line processing
    string seqFile, annFile, exprFile, motifFile, factorExprFile, coopFile, factorInfoFile, repressionFile, parFile;
    string outFile;     // output file
    double coopDistThr = 50;
    double factorIntSigma = 50.0;   // sigma parameter for the Gaussian interaction function
    double repressionDistThr = 250;
    int maxContact = 1;
	double eTF = 0.99;
	int print_obj_func_only_flag = 0; // -b option, no output except the value of quality fucntional
	int print_expression_only_flag = 0; // -d option, no output except the expression
	string free_fix_indicator_filename;
	ExprPredictor::one_qbtm_per_crm = false;
	ExprPar::one_qbtm_per_crm = false;
	ExprFunc::one_qbtm_per_crm = false;
	ExprFunc::DEBUG_ON = false;
    FactorIntType intOption = BINARY;     // type of interaction function
	ExprPredictor::Precision = 5;
    for ( int i = 1; i < argc; i++ ) {
        if ( !strcmp( "-s", argv[ i ] ) )
            seqFile = argv[ ++i ];
        else if ( !strcmp( "-a", argv[ i ] ) )
            annFile = argv[ ++i ];
        else if ( !strcmp( "-b", argv[i] ) )
            print_obj_func_only_flag = atoi( argv[++i] );
        else if ( !strcmp( "-d", argv[i] ) )
            print_expression_only_flag = atoi( argv[++i] );
		else if ( !strcmp( "-e", argv[ i ] ) )
            exprFile = argv[ ++i ];
        else if ( !strcmp( "-m", argv[ i ] ) )
            motifFile = argv[ ++i ];
        else if ( !strcmp( "-f", argv[ i ] ) )
            factorExprFile = argv[ ++i ];
        else if ( !strcmp( "-o", argv[ i ] ) )
            ExprPredictor::modelOption = getModelOption( argv[++i] );
        else if ( !strcmp( "-c", argv[ i ] ) )
            coopFile = argv[ ++i ];
        else if ( !strcmp( "-i", argv[ i ] ) )
            factorInfoFile = argv[ ++i ];
        else if ( !strcmp( "-io", argv[ i ] ) )
            intOption = ( 0 > atoi( argv[++i] )) ? GAUSSIAN : BINARY;
        else if ( !strcmp( "-r", argv[ i ] ) )
            repressionFile = argv[ ++i ];
        else if ( !strcmp( "-oo", argv[ i ] ) )
            ExprPredictor::objOption = getObjOption( argv[++i] );
        else if ( !strcmp( "-mc", argv[i] ) )
            maxContact = atoi( argv[++i] );
        else if ( !strcmp( "-fo", argv[i] ) )
            outFile = argv[++i];
        else if ( !strcmp( "-p", argv[i] ) )
            parFile = argv[++i];
        else if ( !strcmp( "-ct", argv[i] ) )
            coopDistThr = atof( argv[++i] );
        else if ( !strcmp( "-sigma", argv[i] ) )
            factorIntSigma = atof( argv[++i] );
        else if ( !strcmp( "-rt", argv[i] ) )
            repressionDistThr = atof( argv[++i] );
        else if ( !strcmp( "-np", argv[i] ) )
			ExprPredictor::Precision = atoi( argv[++i] );
        else if ( !strcmp( "-nb", argv[i] ) )
			ExprFunc::DEBUG_ON = atoi( argv[++i] );
        else if ( !strcmp( "-ff", argv[i] ) )
            free_fix_indicator_filename = argv[++i];
        else if ( !strcmp( "-oq", argv[i] ) ){
            	ExprPredictor::one_qbtm_per_crm = true;
                ExprPar::one_qbtm_per_crm = true;
                ExprFunc::one_qbtm_per_crm = true;
            }
        else if ( !strcmp( "-et", argv[i] ) )
            eTF = atof( argv[ ++i ] );
    }
    if ( seqFile.empty() || exprFile.empty() || motifFile.empty() || factorExprFile.empty() || ( ( ExprPredictor::modelOption == QUENCHING || ExprPredictor::modelOption == CHRMOD_UNLIMITED || ExprPredictor::modelOption == CHRMOD_LIMITED ) &&  factorInfoFile.empty() ) || ( ExprPredictor::modelOption == QUENCHING && repressionFile.empty() ) ) {
        cerr << "Usage: " << argv[ 0 ] << " -s seqFile -e exprFile -m motifFile -f factorExprFile -fo outFile [-a annFile -o modelOption -c coopFile -i factorInfoFile -r repressionFile -oo objOption -mc maxContact -p parFile -rt repressionDistThr -na nAlternations -ct coopDistThr -sigma factorIntSigma]" << endl;
		cerr << "-b - no output except the value of quality fucntional" << endl;
		cerr << "-nb - non verbose" << endl;
		cerr << "modelOption: Logistic, Direct, Quenching, ChrMod_Unlimited, ChrMod_Limited" << endl;
        exit( 1 );
    }
// additional control parameters
    double gcContent = 0.5;
    ExprPar::searchOption = CONSTRAINED;      // search option: unconstrained; constrained.
    ExprPar::estBindingOption = 1;
    ExprPredictor::min_delta_f_SSE = 1.0E-10;
    ExprPredictor::min_delta_f_Corr = 1.0E-10;
    ExprPredictor::min_delta_f_CrossCorr = 1.0E-10;
    int rval;
    vector< vector< double > > data;    // buffer for reading matrix data
    vector< string > labels;    // buffer for reading the labels of matrix data
    string factor1, factor2;    // buffer for reading factor pairs
// read the sequences
    vector< Sequence > seqs;
    vector< string > seqNames;
    rval = readSequences( seqFile, seqs, seqNames );
    assert( rval != RET_ERROR );
    int nSeqs = seqs.size();

    // read the expression data
    vector< string > condNames;
    rval = readMatrix( exprFile, labels, condNames, data );
    assert( rval != RET_ERROR );
    assert( labels.size() == nSeqs );
    for ( int i = 0; i < nSeqs; i++ ) {
    	if( labels[ i ] != seqNames[ i ] ) cout << labels[i] << seqNames[i] << endl;
        assert( labels[i] == seqNames[i] );
    }
    Matrix exprData( data );
    int nConds = exprData.nCols();

    // read the motifs
    vector< Motif > motifs;
    vector< string > motifNames;
    vector< double > background = createNtDistr( gcContent );
    rval = readMotifs( motifFile, background, motifs, motifNames );
    assert( rval != RET_ERROR );
    int nFactors = motifs.size();

    // factor name to index mapping
    map< string, int > factorIdxMap;
    for ( int i = 0; i < motifNames.size(); i++ ) {
        factorIdxMap[motifNames[i]] = i;
    }

    // read the factor expression data
    labels.clear();
    data.clear();
    rval = readMatrix( factorExprFile, labels, condNames, data );
    assert( rval != RET_ERROR );
   	if ( ExprFunc::DEBUG_ON == 1 ) { // -b in action
        cout << labels.size() << " " << nFactors << " " << condNames.size() << " " << nConds << endl;
	}
    assert( labels.size() == nFactors && condNames.size() == nConds );
    for ( int i = 0; i < nFactors; i++ ) assert( labels[i] == motifNames[i] );
    Matrix factorExprData( data );
    assert( factorExprData.nCols() == nConds );
//initialize the energy threshold factors
	vector < double > energyThrFactors;
	energyThrFactors.clear( );
	for ( int index = 0; index < nFactors; index++ ){
		energyThrFactors.push_back( eTF );
	}
// site representation of the sequences
    vector< SiteVec > seqSites( nSeqs );
    vector< int > seqLengths( nSeqs );
    SeqAnnotator ann( motifs, energyThrFactors );
    if ( annFile.empty() ) {        // construct site representation
        for ( int i = 0; i < nSeqs; i++ ) {
//cout << "Annotated sites for CRM: " << seqNames[i] << endl;
            	ann.annot( seqs[ i ], seqSites[ i ] );
            	seqLengths[i] = seqs[i].size();
        }
    } else {    // read the site representation and compute the energy of sites
        rval = readSites( annFile, factorIdxMap, seqSites, true );
        assert( rval != RET_ERROR );
        for ( int i = 0; i < nSeqs; i++ ) {
            ann.compEnergy( seqs[i], seqSites[i] );
            seqLengths[i] = seqs[i].size();
        }
    }
    if ( ExprFunc::DEBUG_ON ) {
        for ( int i = 0; i < nSeqs; i++ ) {
            cout << "Sequence number " << i << " name " << seqNames[i] << " length " << seqSites[i].size() << endl;
            cout << "Site nummber" << "\t" << "Start + 1" << "\t" << "Strand" << "\t" << "Factor Idx" << "\t" << "Energy" << "\t" << "Wt Ratio" << "\t" << "word" << endl;
            for ( int j = 0; j < seqSites[i].size(); j++ ) {
                cout << j << "\t" << seqSites[i][j]; //.energy << " wtRatio " << seqSites[i][j].wtRatio;
//#ifdef ADEBUG
                cout << "\t" << seqSites[i][j].word << endl;
//#else
//                cout << endl;
//#endif
            }
        }
    }
    // read the cooperativity matrix
    int num_of_coop_pairs = 0;
    IntMatrix coopMat( nFactors, nFactors, false );
    if ( !coopFile.empty() ) {
        ifstream fcoop( coopFile.c_str() );
        if ( !fcoop ) {
            cerr << "Cannot open the cooperativity file " << coopFile << endl;
            exit( 1 );
        }
        while ( fcoop >> factor1 >> factor2 ) {
            assert( factorIdxMap.count( factor1 ) && factorIdxMap.count( factor2 ) );
            int idx1 = factorIdxMap[factor1];
            int idx2 = factorIdxMap[factor2];
            if( coopMat( idx1, idx2 ) == false && coopMat( idx2, idx1 ) == false ) {
                coopMat( idx1, idx2 ) = true;
                coopMat( idx2, idx1 ) = true;
                num_of_coop_pairs ++;
            }
        }
    }

    // read the roles of factors
    vector< bool > actIndicators( nFactors, false );
    vector< bool > repIndicators( nFactors, false );
    if ( !factorInfoFile.empty() ) {
        ifstream finfo( factorInfoFile.c_str() );
        if ( !finfo ) {
            cerr << "Cannot open the factor information file " << factorInfoFile << endl;
            exit( 1 );
        }
        string name;
        int i = 0, actRole, repRole;
        while ( finfo >> name >> actRole >> repRole ) {
            assert( name == motifNames[i] );
            if ( actRole ) actIndicators[i] = true;
            if ( repRole ) repIndicators[i] = true;
            i++;
        }
    }

    // read the repression matrix
    IntMatrix repressionMat( nFactors, nFactors, false );
    if ( !repressionFile.empty() ) {
        ifstream frepr( repressionFile.c_str() );
        if ( !frepr ) {
            cerr << "Cannot open the repression file " << repressionFile << endl;
            exit( 1 );
        }
        while ( frepr >> factor1 >> factor2 ) {
            assert( factorIdxMap.count( factor1 ) && factorIdxMap.count( factor2 ) );
            int idx1 = factorIdxMap[factor1];
            int idx2 = factorIdxMap[factor2];
            repressionMat( idx1, idx2 ) = true;
        }
    }

	vector <bool> indicator_bool;
	indicator_bool.clear();
	if( !free_fix_indicator_filename.empty() ){
		ifstream free_fix_indicator_file ( free_fix_indicator_filename.c_str() );
		while( !free_fix_indicator_file.eof( ) ){
			int indicator_var;
			free_fix_indicator_file >> indicator_var;
			assert ( indicator_var == 0 || indicator_var == 1 );
			indicator_bool.push_back( indicator_var );
		}
	} else {
		//for binding weights, coop pairs and transcriptional effects
		for( int index = 0; index < nFactors + num_of_coop_pairs + nFactors; index++ ){
			indicator_bool.push_back( true );
		}
		if( ExprPredictor::one_qbtm_per_crm ){
			for( int index = 0; index < nSeqs; index++ ){
				indicator_bool.push_back( true );
			}
		} else {
			indicator_bool.push_back( true );
		}
	}
//#ifdef ADEBUG
	if ( ExprFunc::DEBUG_ON == 1 ) { // -b in action
    // CHECK POINT
     cout << "Sequences:" << endl;
     for ( int i = 0; i < seqs.size(); i++ ) cout << seqNames[i] << endl << seqs[i] << endl;
     cout << "Expression: " << endl << exprData << endl;
     cout << "Factor motifs:" << endl;
     for ( int i = 0; i < motifs.size(); i++ ) cout << motifNames[i] << endl << motifs[i] << endl;
     cout << "Factor expression:" << endl << factorExprData << endl;
     cout << "Cooperativity matrix:" << endl << coopMat << endl;
     cout << "Activators:" << endl << actIndicators << endl;
     cout << "Repressors:" << endl << repIndicators << endl;
     cout << "Repression matrix:" << endl << repressionMat << endl;
     cout << "Site representation of sequences:" << endl;
     for ( int i = 0; i < nSeqs; i++ ) {
         cout << ">" << seqNames[i] << endl;
         for ( int j = 0; j < seqSites[i].size(); j++ ) cout << seqSites[i][j] << endl;
     }
    // print the parameters for running the analysis
    cout << "Parameters for running the program: " << endl;
    cout << "Model = " << getModelOptionStr( ExprPredictor::modelOption ) << endl;
    if ( ExprPredictor::modelOption == QUENCHING || ExprPredictor::modelOption == CHRMOD_LIMITED ) {
        cout << "Maximum_Contact = " << maxContact << endl;
    }
    if ( ExprPredictor::modelOption == QUENCHING || ExprPredictor::modelOption == CHRMOD_LIMITED || ExprPredictor::modelOption == CHRMOD_UNLIMITED ) {
        cout << "Repression_Distance_Threshold = " << repressionDistThr << endl;
    }
    cout << "Objective_Function = " << getObjOptionStr( ExprPredictor::objOption ) << endl;
    if ( !coopFile.empty() ) {
        cout << "Interaction_Model = " << getIntOptionStr( intOption ) << endl;
        cout << "Interaction_Distance_Threshold = " << coopDistThr << endl;
        if ( intOption == GAUSSIAN ) cout << "Sigma = " << factorIntSigma << endl;
    }
    cout << "Search_Option = " << getSearchOptionStr( ExprPar::searchOption ) << endl;
    cout << "Energy Threshold Factor = " << eTF << endl;
	} // -b in action
//#endif
	// create the expression predictor
    FactorIntFunc* intFunc;
    if ( intOption == BINARY ) intFunc = new FactorIntFuncBinary( coopDistThr );
    else if ( intOption == GAUSSIAN ) intFunc = new FactorIntFuncGaussian( coopDistThr, factorIntSigma );
    else {
        cerr << "Interaction Function is invalid " << endl; exit( 1 );
    }
    ExprPredictor* predictor = new ExprPredictor( seqNames, seqSites, seqLengths, exprData, motifs, factorExprData, intFunc, coopMat, actIndicators, maxContact, repIndicators, repressionMat, repressionDistThr, indicator_bool, motifNames);
    // read the initial parameter values
    ExprPar par_init( nFactors, nSeqs );
    if ( !parFile.empty() ) {
        rval = par_init.load( parFile );
        if ( rval == RET_ERROR ) {
            cerr << "Cannot read parameters from " << parFile << endl;
            exit( 1 );
        }
    }
	if ( print_expression_only_flag == 1 ) { // -d in action
        predictor->expressionPrint( par_init );
        return 0;
	} // -d in action
	if ( print_obj_func_only_flag == 1 ) { // -b in action
        predictor->score( par_init );
        return 0;
	} // -b in action
    return 0;
}

