#ifndef EXPR_PREDICTOR_H
#define EXPR_PREDICTOR_H

#include "SeqAnnotator.h"
#include "ExprPar.h"
#include "ExprFunc.h"

enum ObjType {
    SSE,    // sum of squared error
    CORR,   // Pearson correlation
    CROSS_CORR  // cross correlation (maximum in a range of shifts)
};

ObjType getObjOption( const string& objOptionStr );
string getObjOptionStr( ObjType objOption );

/*****************************************************
* Model Training and Testing
******************************************************/

/* ExprPredictor class: the thermodynamic sequence-to-expression predictor */
class ExprPredictor {
public:
    // constructors
    ExprPredictor( const vector< string >& _seqNames, const vector< SiteVec >& _seqSites, const vector< int >& _seqLengths, Matrix& _exprData, const vector< Motif >& _motifs, Matrix& _factorExprData, const FactorIntFunc* _intFunc, const IntMatrix& _coopMat, const vector< bool >& _actIndicators, int _maxContact, const vector< bool >& _repIndicators, const IntMatrix& _repressionMat, double _repressionDistThr, const vector < bool >& _indicator_bool, const vector <string>& _motifNames);

    // access methods
    int nSeqs() const {
        return seqSites.size();
    }
    int nFactors() const {
        return motifs.size();
    }
    int nConds() const {
         return exprData.nCols();
    }
    const IntMatrix& getCoopMat() const {
        return coopMat;
    }
    const vector< bool >& getActIndicators() const {
        return actIndicators;
    }
    const vector< bool >& getRepIndicators() const {
        return repIndicators;
    }
    const IntMatrix& getRepressionMat() const {
        return repressionMat;
    }
    const ExprPar& getPar() const { return par_model; }
    double getObj() const { return obj_model; }

    // the objective function to be minimized
    double objFunc( const ExprPar& par ) ;

    // training the model
	int score( const ExprPar& par_init ); // print objectives, don't train
	int expressionPrint( const ExprPar& par ); // get expression
    // predict expression values of a sequence (across the same conditions)
	int predict( const ExprPar& par, const SiteVec& targetSites, int targetSeqLength, vector< double >& targetExprs, int seq_num ) const;
	static int Precision;
    static ModelType modelOption;     // model option
    static int estBindingOption;    // whether estimate binding parameters
    static ObjType objOption;       // option of the objective function

    // the similarity between two expression patterns, using cross-correlation
    static double exprSimCrossCorr( const vector< double >& x, const vector< double >& y );
    static int maxShift;    // maximum shift when computing cross correlation
    static double shiftPenalty;     // the penalty for shift (when weighting different positions)

    // the parameters for the optimizer
    static double min_delta_f_SSE;      // the minimum change of the objective function under SSE
    static double min_delta_f_Corr;     // the minimum change of the objective function under correlation
    static double min_delta_f_CrossCorr;    // the minimum change of the objective function under cross correlation
    static bool one_qbtm_per_crm;
    vector < bool > indicator_bool;
    vector <string> motifNames;
    vector < double > fix_pars;
    vector < double > free_pars;
private:
    // training data
    const vector< SiteVec >& seqSites;		// the extracted sites for all sequences
    const vector< int >& seqLengths;           // lengths of all sequences
    const vector< string >& seqNames;       // names of sequences
    Matrix& exprData;		// expressions of the corresponding sequences across multiple conditions
    Matrix& exprDataCopy;		// expressions of the corresponding sequences across multiple conditions
    const vector< Motif >& motifs;		// TF binding motifs
    Matrix& factorExprData;		// [TF] of all factors over multiple conditions
    Matrix& factorExprDataCopy;		// [TF] of all factors over multiple conditions

    // control parameters
    const FactorIntFunc* intFunc;   // function to compute distance-dependent TF-TF interactions
    const IntMatrix& coopMat;       // cooperativity matrix: C(f,f') = 1 if f and f' bind cooperatively
    const vector< bool >& actIndicators;   // 1 if the TF is in the activator set
    int maxContact;     // the maximum contact
    const vector< bool >& repIndicators;    // 1 if the TF is in the repressor set
    const IntMatrix& repressionMat;    // repression matrix: R(f,f') = 1 if f can repress f'
    double repressionDistThr;   // distance threshold for repression: d_R

    // model parameters and the value of the objective function
    ExprPar par_model;
    double obj_model;

    // print the parameter values (the ones that are estimated) in a single line
    void printPar( const ExprPar& par ) const;

    // create the expression function
    ExprFunc* createExprFunc( const ExprPar& par ) const;

    // objective functions
    double compRMSE( const ExprPar& par );		// root mean square error between predicted and observed expressions
    double compAvgCorr( const ExprPar& par );     // the average Pearson correlation
    double compAvgCrossCorr( const ExprPar& par );    // the average cross correlation -based similarity
	vector< double > compAbstr( const ExprPar& par ); // get all scores in a vector
};

#endif
