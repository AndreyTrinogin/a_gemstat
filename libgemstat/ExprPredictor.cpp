#include <gsl/gsl_math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_sf_exp.h>

#include "ExprPar.h"
#include "ExprFunc.h"
#include "ExprPredictor.h"

ObjType getObjOption( const string& objOptionStr )
{
    if ( toupperStr( objOptionStr ) == "SSE" ) return SSE;
    if ( toupperStr( objOptionStr ) == "CORR" ) return CORR;
    if ( toupperStr( objOptionStr ) == "CROSS_CORR" ) return CROSS_CORR;

    cerr << "objOptionStr is not a valid option of objective function" << endl;
    exit(1);
}

string getObjOptionStr( ObjType objOption )
{
    if ( objOption == SSE ) return "SSE";
    if ( objOption == CORR ) return "Corr";
    if ( objOption == CROSS_CORR ) return "Cross_Corr";

    return "Invalid";
}

ModelType ExprPar::modelOption = CHRMOD_UNLIMITED;
SearchType ExprPar::searchOption = UNCONSTRAINED;
int ExprPar::estBindingOption = 1;  // 1. estimate binding parameters; 0. not estimate binding parameters
double ExprPar::default_weight = 1.0;
double ExprPar::default_interaction = 1.0;
double ExprPar::default_effect_Logistic = 0.0;
double ExprPar::default_effect_Thermo = 1.0;
double ExprPar::default_repression = 1.0E-2;
double ExprPar::default_basal_Logistic = -5.0;
double ExprPar::default_basal_Thermo = 0.01;
double ExprPar::min_weight = 0.01;
double ExprPar::max_weight = 100;//500;
double ExprPar::min_interaction = 0.01;
double ExprPar::max_interaction = 100;//500;
double ExprPar::min_effect_Logistic = -5;
double ExprPar::max_effect_Logistic = 5;
double ExprPar::min_effect_Thermo = 0.01;
double ExprPar::max_effect_Thermo = 500;
double ExprPar::min_repression = 1.0E-3;
double ExprPar::max_repression = 500;
double ExprPar::min_basal_Logistic = -9.0;
double ExprPar::max_basal_Logistic = -1.0;
double ExprPar::min_basal_Thermo = 1.0E-4;
double ExprPar::max_basal_Thermo = 0.1;
double ExprPar::delta = 0.0001;
bool ExprPar::one_qbtm_per_crm = false;

bool ExprFunc::one_qbtm_per_crm = false;

int ExprFunc::seq_index = 0;

ModelType ExprFunc::modelOption = QUENCHING;
bool ExprFunc::DEBUG_ON = false;

ExprPredictor::ExprPredictor( const vector< string >& _seqNames, const vector< SiteVec >& _seqSites, const vector< int >& _seqLengths, Matrix& _exprData, const vector< Motif >& _motifs, Matrix& _factorExprData, const FactorIntFunc* _intFunc, const IntMatrix& _coopMat, const vector< bool >& _actIndicators, int _maxContact, const vector< bool >& _repIndicators, const IntMatrix& _repressionMat, double _repressionDistThr, const vector < bool >& _indicator_bool, const vector <string>& _motifNames) :
            seqNames(_seqNames), seqSites( _seqSites ), seqLengths( _seqLengths ), exprData( _exprData ), exprDataCopy( _exprData ), motifs( _motifs ), factorExprData( _factorExprData ), factorExprDataCopy( _factorExprData ), intFunc( _intFunc ), coopMat( _coopMat ), actIndicators( _actIndicators ), maxContact( _maxContact ), repIndicators( _repIndicators ), repressionMat( _repressionMat ), repressionDistThr( _repressionDistThr ), indicator_bool ( _indicator_bool ), motifNames ( _motifNames )
{
    assert( exprData.nRows() == nSeqs() );
    assert( factorExprData.nRows() == nFactors() && factorExprData.nCols() == nConds() );
    assert( coopMat.isSquare() && coopMat.isSymmetric() && coopMat.nRows() == nFactors() );
    assert( actIndicators.size() == nFactors() );
    assert( maxContact > 0 );
    assert( repIndicators.size() == nFactors() );
    assert( repressionMat.isSquare() && repressionMat.nRows() == nFactors() );
    assert( repressionDistThr >= 0 );

    // set the model option for ExprPar and ExprFunc
    ExprPar::modelOption = modelOption;
    ExprFunc::modelOption = modelOption;

    // set the values of the parameter range according to the model option
    if ( modelOption != LOGISTIC && modelOption != DIRECT ) {
        ExprPar::min_effect_Thermo = 0.99;
        ExprPar::min_interaction = 0.99;
    }

    // set the option of parameter estimation
    ExprPar::estBindingOption = estBindingOption;
}

double ExprPredictor::objFunc( const ExprPar& par )
{
    if ( objOption == SSE ) return compRMSE( par );
    if ( objOption == CORR ) return -compAvgCorr( par );
    if ( objOption == CROSS_CORR ) return -compAvgCrossCorr( par );
}

int ExprPredictor::score( const ExprPar& par_init )
{
	int i, length;
    par_model = par_init;
    if ( ExprFunc::DEBUG_ON == 1 ) {
        cout << "*** Diagnostic printing BEFORE adjust() ***" << endl;
        cout << "Parameters: " << endl;
        printPar( par_model );
        cout << endl;
        cout << "Objective function values: ";
        cout << endl;
    }
    vector< double > scores = compAbstr( par_model );
    cout << setprecision( Precision );
	length = scores.size();
	cout << "RMSE = " << scores[length - 3] << ";" ;
	if ( nConds() > maxShift ) {
        cout << " AvgCorr = " << scores[length - 2] << "; AvgCrossCorr = " << scores[length - 1] << ";" << endl;
	} else {
        cout << endl;
    }
	for ( int i = 0 , j = 0; i < length - 3 ; i += 3, j++ ) {
		cout << seqNames[j] << " RSS = " << scores[i] << ";" ;
        if ( nConds() > maxShift ) {
            cout << " Corr = " << scores[i + 1] << "; CrossCorr = " << scores[i + 2] << ";" << endl;
        } else {
            cout << endl;
        }
	}
    if ( ExprFunc::DEBUG_ON == 1 ) {
        cout << "*******************************************" << endl << endl;
	}
	return 0;
}

ModelType ExprPredictor::modelOption = LOGISTIC;
int ExprPredictor::estBindingOption = 1;    // 1. estimate binding parameters; 0. not estimate binding parameters
ObjType ExprPredictor::objOption = SSE;

double ExprPredictor::exprSimCrossCorr( const vector< double >& x, const vector< double >& y )
{
    if ( x.size() < maxShift || y.size() < maxShift ) return 0;
    vector< int > shifts;
    for ( int s = -maxShift; s <= maxShift; s++ ) {
        shifts.push_back( s );
    }

    vector< double > cov;
    vector< double > corr;
    cross_corr( x, y, shifts, cov, corr );
    double result = 0, weightSum = 0;
//     result = corr[maxShift];
    result = *max_element( corr.begin(), corr.end() );
//     for ( int i = 0; i < shifts.size(); i++ ) {
//         double weight = pow( shiftPenalty, abs( shifts[i] ) );
//         weightSum += weight;
//         result += weight * corr[i];
//     }
//     result /= weightSum;

    return result;
}

int ExprPredictor::maxShift = 5;
double ExprPredictor::shiftPenalty = 0.8;

double ExprPredictor::min_delta_f_SSE = 1.0E-8;
double ExprPredictor::min_delta_f_Corr = 1.0E-8;
double ExprPredictor::min_delta_f_CrossCorr = 1.0E-8;
bool ExprPredictor::one_qbtm_per_crm = false;
int ExprPredictor::Precision = 5;

ExprFunc* ExprPredictor::createExprFunc( const ExprPar& par ) const
{
	return new ExprFunc( intFunc, actIndicators, maxContact, repIndicators, repressionMat, repressionDistThr, par );
}

int ExprPredictor::predict( const ExprPar& par, const SiteVec& targetSites, int targetSeqLength, vector< double >& targetExprs, int seq_num ) const
{
    targetExprs.clear();
    // predict the expression
    ExprFunc* func = createExprFunc( par );
    for ( int j = 0; j < nConds(); j++ ) {
        vector< double > concs = factorExprData.getCol( j );
        double predicted = func->predictExpr( targetSites, targetSeqLength, concs, seq_num, 0 );
        targetExprs.push_back( predicted );
    }

    return 0;
}

int ExprPredictor::expressionPrint( const ExprPar& par )
{
    vector< double > predictedExprs;
    for ( int i = 0; i < nSeqs(); i++ ) {
        predict( par, seqSites[i], seqLengths[i], predictedExprs, i );
        cout << seqNames[i];
        for ( int j = 0; j < nConds(); j++ ) {
        	cout << "\t" << predictedExprs[j];
        }
        cout << endl;
    }
	return 0;
}

vector< double > ExprPredictor::compAbstr( const ExprPar& par )
{
	vector< double > scores;
    double squaredErr = 0;
    double totalSim = 0;
    double totalCrossSim = 0;
    double squaredErrTot = 0;
    double totalSimTot = 0;
    double totalCrossSimTot = 0;
    for ( int i = 0; i < nSeqs(); i++ ) {
        vector< double > predictedExprs;
        predict( par, seqSites[i], seqLengths[i], predictedExprs, i );
        vector< double > observedExprs = exprData.getRow( i );
        double beta;
        if ( observedExprs.size() > 1 ) {
            squaredErr = least_square( predictedExprs, observedExprs );
            if ( nConds() > maxShift ) {
                totalSim = corr( predictedExprs, observedExprs );
                totalCrossSim = exprSimCrossCorr( predictedExprs, observedExprs );
            } else {
                totalSim = 0;
                totalCrossSim = 0;
            }
        } else {
            squaredErr = (predictedExprs[0] - observedExprs[0]) * (predictedExprs[0] - observedExprs[0]);
            totalSim = 0;
            totalCrossSim = 0;
        }
    	scores.push_back ( squaredErr );
        scores.push_back ( -totalSim );
        scores.push_back ( -totalCrossSim );
        squaredErrTot += squaredErr;
        totalSimTot += totalSim;
        totalCrossSimTot += totalCrossSim;
    }
    scores.push_back ( squaredErrTot );
    scores.push_back ( -totalSimTot / nSeqs() );
    scores.push_back ( -totalCrossSimTot / nSeqs() );
    return scores;
}

double ExprPredictor::compRMSE( const ExprPar& par )
{
    // create the expression function
    ExprFunc* func = createExprFunc( par );

    // error of each sequence
    double squaredErr = 0;
    for ( int i = 0; i < nSeqs(); i++ ) {
        vector< double > predictedExprs;
        vector< double > observedExprs;
        for ( int j = 0; j < nConds(); j++ ) {
		double predicted = -1;
            	vector< double > concs = factorExprData.getCol( j );
		predicted = func->predictExpr( seqSites[ i ], seqLengths[i], concs, i, 0 );

            // predicted expression for the i-th sequence at the j-th condition
            predictedExprs.push_back( predicted );

            // observed expression for the i-th sequence at the j-th condition
            double observed = exprData( i, j );
            observedExprs.push_back( observed );
        }
        double beta;
        squaredErr += least_square( predictedExprs, observedExprs, beta );
    }

    double rmse = sqrt( squaredErr / ( nSeqs() * nConds() ) );
    return rmse;
}

double ExprPredictor::compAvgCorr( const ExprPar& par )
{
    // create the expression function
    ExprFunc* func = createExprFunc( par );

    // Pearson correlation of each sequence
    double totalSim = 0;
    for ( int i = 0; i < nSeqs(); i++ ) {
        vector< double > predictedExprs;
        vector< double > observedExprs;
        for ( int j = 0; j < nConds(); j++ ) {
		double predicted = -1;
            	vector< double > concs = factorExprData.getCol( j );
		predicted = func->predictExpr( seqSites[ i ], seqLengths[ i ], concs, i, 0 );

            // predicted expression for the i-th sequence at the j-th condition
            predictedExprs.push_back( predicted );

            // observed expression for the i-th sequence at the j-th condition
            double observed = exprData( i, j );
            observedExprs.push_back( observed );
        }
        totalSim += corr( predictedExprs, observedExprs );
//         cout << "Sequence " << i << "\t" << corr( predictedExprs, observedExprs ) << endl;
    }

    return totalSim / nSeqs();
}

double ExprPredictor::compAvgCrossCorr( const ExprPar& par )
{
    // create the expression function
    ExprFunc* func = createExprFunc( par );

    // cross correlation similarity of each sequence
    double totalSim = 0;
    for ( int i = 0; i < nSeqs(); i++ ) {
        vector< double > predictedExprs;
        vector< double > observedExprs;
        for ( int j = 0; j < nConds(); j++ ) {
		double predicted = -1;
            	vector< double > concs = factorExprData.getCol( j );
		predicted = func->predictExpr( seqSites[ i ], seqLengths[i], concs, i, 0 );

            // predicted expression for the i-th sequence at the j-th condition
            predictedExprs.push_back( predicted );

            // observed expression for the i-th sequence at the j-th condition
            double observed = exprData( i, j );
            observedExprs.push_back( observed );
        }
        totalSim += exprSimCrossCorr( predictedExprs, observedExprs );
    }

    return totalSim / nSeqs();
}


void ExprPredictor::printPar( const ExprPar& par ) const
{
    cout.setf( ios::fixed );
// print binding weights
    if ( estBindingOption ) {
        for ( int i = 0; i < nFactors(); i++ ) {
            cout << par.maxBindingWts[i] << "\t";
        }
    }
// print the interaction matrix
    for ( int i = 0; i < nFactors(); i++ ) {
        for ( int j = 0; j <= i; j++ ) {
            if ( coopMat( i, j ) ) cout << par.factorIntMat( i, j ) << "\t";
        }
    }
// print the transcriptional effects
	int n_seqs = nSeqs();
	int n_factors = nFactors();
    for ( int i = 0; i < nFactors(); i++ ) {
        if ( modelOption == LOGISTIC || modelOption == DIRECT ) {
       		cout << par.txpEffects[i] << "\t";
        } else {
            if ( actIndicators[i] ) {
       		cout << par.txpEffects[i] << "\t";
            }
        }
    }
// print the repression effects
    if ( modelOption == CHRMOD_UNLIMITED || modelOption == CHRMOD_LIMITED ) {
        for ( int i = 0; i < nFactors(); i++ ) {
            if ( repIndicators[i] ) cout << par.repEffects[i] << "\t";
        }
    }
// print the basal transcriptions
    for( int _i = 0; _i < par.basalTxps.size(); _i ++ ){
    	cout << par.basalTxps[ _i ] << "\t";
    }
//    cout << endl;
}
