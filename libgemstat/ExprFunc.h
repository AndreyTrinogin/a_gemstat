#ifndef EXPR_FUNC_H
#define EXPR_FUNC_H

#include "SeqAnnotator.h"
#include "ExprPar.h"

enum FactorIntType {
    BINARY,     // Binary model of interaction
    GAUSSIAN    // Gaussian model of interaction
}; 

string getIntOptionStr( FactorIntType intOption );

/*****************************************************
* Factor-Factor Interactions
******************************************************/

/* FactorIntFunc class: distance-dependent function of TF-TF interaction  */
class FactorIntFunc {
public:
    // compute the factor interaction, given the normal interaction (when they are close enough)
    virtual double compFactorInt( double normalInt, double dist, bool orientation ) const = 0;

    // the maximum distance beyond which there is no interaction
    virtual double getMaxDist() const = 0;    
};

/* FactorIntFuncBinary class: binary distance function */
class FactorIntFuncBinary : public FactorIntFunc {
public:
    // constructors
    FactorIntFuncBinary( double _distThr, double _orientationEffect = 1.0 ) : distThr( _distThr ), orientationEffect( _orientationEffect ) { assert( distThr > 0 ); }

    // compute the factor interaction
    double compFactorInt( double normalInt, double dist, bool orientation ) const;

    // the maximum distance beyond which there is no interaction
    double getMaxDist() const {
        return distThr;
    } 
private:
    double distThr;		// if distance < thr, the "normal" value; otherwise 1 (no interaction)
    double orientationEffect;	// the effect of orientation: if at different strands, the effect should be multiplied this value	
};

/* FactorIntFuncGaussian class: Gaussian distance function*/
class FactorIntFuncGaussian : public FactorIntFunc {
public: 
    // constructors
    FactorIntFuncGaussian( double _distThr, double _sigma ) : distThr( _distThr ), sigma( _sigma ) {
        assert( distThr > 0 && sigma > 0 );
    }

    // compute the factor interaction
    double compFactorInt( double normalInt, double dist, bool orientation ) const; 

    // the maximum distance beyone which there is no interaction
    double getMaxDist() const {
        return distThr;
    } 
private: 
    double distThr;     // no interaction if distance is greater than thr. 
    double sigma;       // standard deviation of 
};

/* FactorIntFuncGeometric class: distance function decays geometrically (but never less than 1) */
class FactorIntFuncGeometric : public FactorIntFunc {
public:
    // constructors
    FactorIntFuncGeometric( double _distThr, double _spacingEffect, double _orientationEffect ) : distThr( _distThr ), spacingEffect( _spacingEffect ), orientationEffect( _orientationEffect ) { assert( distThr > 0 ); }

    // compute the factor interaction
    double compFactorInt( double normalInt, double dist, bool orientation ) const;

    // the maximum distance beyond which there is no interaction
    double getMaxDist() const {
        return distThr;
    } 
private:
    double distThr;		// if distance < thr, the "normal" value; otherwise decay with distance (by parameter spacingEffect)
    double spacingEffect;		// the effect of spacing
    double orientationEffect;	// the effect of orientation: if at different strands, the effect should be multiplied this value
};

/* ExprFunc class: predict the expression (promoter occupancy) of an enhancer sequence */
class ExprFunc {
public:
    // constructors
	ExprFunc( const FactorIntFunc* _intFunc, const vector< bool >& _actIndicators, int _maxContact, const vector< bool >& _repIndicators, const IntMatrix& _repressionMat, double _repressionDistThr, const ExprPar& _par );
    // access methods
//    const vector< Motif >& getMotifs() const {
//        return motifs;
//    }
    
    // predict the expression value of a given sequence (its site representation, sorted by the start positions) under given TF concentrations
    double predictExpr( const SiteVec& _sites, int length, const vector< double >& factorConcs, int seq_num, int out_flag );
    double predictExpr( const SiteVec& _sites, int length, const vector< double >& factorConcs, int seq_num, int out_flag, double *zOn, double *zOff );
    static ModelType modelOption;     // model option   
    static bool one_qbtm_per_crm;
    static int seq_index;
    static bool DEBUG_ON;
private:
    // TF binding motifs
//    const vector< Motif >& motifs;
	int nFactors;
    // control parameters
    const FactorIntFunc* intFunc;   // function to compute distance-dependent TF-TF interactions     
    const vector< bool >& actIndicators;    // 1 if the TF is in the activator set
    int maxContact;     // the maximum contact     
    const vector< bool >& repIndicators;    // 1 if the TF is in the repressor set
    const IntMatrix& repressionMat;    // repression matrix: R(f,f') = 1 if f can repress f'
    double repressionDistThr;   // distance threshold for repression: d_R
			
    // model parameters
    const ExprPar& par;
		    
    // the sequence whose expression is to be predicted
    SiteVec sites;
    vector< int > boundaries;   // left boundary of each site beyond which there is no interaction    

    // intermediate computational results
    vector< double > bindingWts; 
		
    // compute the partition function when the basal transcriptional machinery (BTM) is not bound
    double compPartFuncOff() const;

	// compute the partition function when the BTM is not bound: ChrMod model 
    double compPartFuncOffChrMod() const; 
    
    // compute the partition function when the BTM is bound 
    double compPartFuncOn() const;

    // compute the paritition function when the BTM is bound: Direct model
    double compPartFuncOnDirect() const;
    
    // compute the paritition function when the BTM is bound: Quenching model
    double compPartFuncOnQuenching() const;

     // compute the paritition function when the BTM is bound: ChrMod_Unlimited model
    double compPartFuncOnChrMod_Unlimited() const;

     // compute the paritition function when the BTM is bound: ChrMod_Limited model
    double compPartFuncOnChrMod_Limited() const;    
    
    // compute the TF-TF interaction between two occupied sites
    double compFactorInt( const Site& a, const Site& b ) const;

    // test if one site represses another site
    bool testRepression( const Site& a, const Site& b ) const;
};

#endif
