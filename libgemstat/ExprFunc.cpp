#include <gsl/gsl_math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_sf_exp.h>

#include "ExprPar.h"
#include "ExprFunc.h"

/*
bool ExprFunc::one_qbtm_per_crm = false;
int ExprFunc::seq_index = 0;
ModelType ExprFunc::modelOption = QUENCHING;
*/

string getIntOptionStr( FactorIntType intOption )
{
    if ( intOption == BINARY ) return "Binary";
    if ( intOption == GAUSSIAN ) return "Gaussian";

    return "Invalid";
}

double FactorIntFuncBinary::compFactorInt( double normalInt, double dist, bool orientation ) const
{
    assert( dist >= 0 );

    double spacingTerm = ( dist < distThr ? normalInt : 1.0 );
    double orientationTerm = orientation ? 1.0 : orientationEffect;
    return spacingTerm * orientationTerm;
}

double FactorIntFuncGaussian::compFactorInt( double normalInt, double dist, bool orientation ) const
{
    assert( dist >= 0 );

    double GaussianInt = dist < distThr ? normalInt * exp( - ( dist * dist ) / ( 2.0 * sigma * sigma ) ) : 1.0;
    return max( 1.0, GaussianInt );
}

double FactorIntFuncGeometric::compFactorInt( double normalInt, double dist, bool orientation ) const
{
    assert( dist >= 0 );

    double spacingTerm = max( 1.0, dist <= distThr ? normalInt : normalInt * pow( spacingEffect, dist - distThr ) );
    double orientationTerm = orientation ? 1.0 : orientationEffect;
    return spacingTerm * orientationTerm;
}

ExprFunc::ExprFunc( const FactorIntFunc* _intFunc, const vector< bool >& _actIndicators, int _maxContact, const vector< bool >& _repIndicators, const IntMatrix& _repressionMat, double _repressionDistThr, const ExprPar& _par ) :  intFunc( _intFunc ), actIndicators( _actIndicators ), maxContact( _maxContact ), repIndicators( _repIndicators ), repressionMat( _repressionMat ), repressionDistThr( _repressionDistThr ), par( _par )
{
	nFactors = par.nFactors();
	assert( actIndicators.size() == nFactors );
	assert( repIndicators.size() == nFactors );
	assert( repressionMat.isSquare() && repressionMat.nRows() == nFactors );
	assert( maxContact >= 0 );
}

double ExprFunc::predictExpr( const SiteVec& _sites, int length, const vector< double >& factorConcs, int seq_num, int out_flag)
{
    bindingWts.clear();
    boundaries.clear();
	seq_index = seq_num;
	if( !one_qbtm_per_crm )
		seq_num = 0;
// store the sequence
    int n = _sites.size();
    sites = _sites;
    sites.insert( sites.begin(), Site() );  // start with a pseudo-site at position 0
    boundaries.push_back( 0 );
    double range = max( intFunc->getMaxDist(), repressionDistThr );
    for ( int i = 1; i <= n; i++ ) {
        int j;
        for ( j = i - 1; j >= 1; j-- ) {
            if ( ( sites[i].start - sites[j].start ) > range ) break;
        }
        int boundary = j;
        boundaries.push_back( boundary );
    }
// compute the Boltzman weights of binding for all sites
    bindingWts.push_back( 1.0 );
    for ( int i = 1; i <= n; i++ ) {
        bindingWts.push_back( par.maxBindingWts[ sites[i].factorIdx ] * factorConcs[sites[i].factorIdx] * sites[i].wtRatio );
    }
// Logistic model
//    int n_factors = motifs.size();
    if ( modelOption == LOGISTIC ) {
		vector< double > factorOcc( nFactors, 0 ); // total occupancy of each factor
        for ( int i = 1; i < sites.size(); i++ ) {
            factorOcc[ sites[i].factorIdx ] += bindingWts[i] / ( 1.0 + bindingWts[i] );
        }
        double totalEffect = 0;
		for ( int i = 0; i < nFactors; i++ ) {
            double effect = par.txpEffects[i] * factorOcc[i];
            totalEffect += effect;
// length correction
//             totalEffect = totalEffect / (double)length;
        }
//         return par.expRatio * logistic( log( par.basalTxp ) + totalEffect );
//#ifdef ADEBUG
		if ( DEBUG_ON ) {
			cout << "Logistic factor\toccupancy\ttxp_effect" << endl;
			for ( int i = 0; i < nFactors; i++ ) {
				cout << i << "\t" << factorOcc[i] << "\t" << par.txpEffects[i] * factorOcc[i] << endl;
			}
			cout << totalEffect << endl;
		}
//#endif
        return logistic( par.basalTxps[ seq_num ] + totalEffect );
    }
// Thermodynamic models: Direct, Quenching, ChrMod_Unlimited and ChrMod_Limited
// compute the partition functions
	double Z_off = compPartFuncOff();
	double Z_on = compPartFuncOn();
//#ifdef ADEBUG
	if ( DEBUG_ON ) {
		cout << "Z_off(" << seq_num << ") = " << Z_off << " Z_on = " << Z_on;
	}
//#endif
// compute the expression (promoter occupancy)
	double efficiency = Z_on / Z_off;
	double promoterOcc = efficiency * par.basalTxps[ seq_num ] / ( 1.0 + efficiency * par.basalTxps[ seq_num ] );
//#ifdef ADEBUG
	if ( DEBUG_ON ) {
		cout << " efficiency(Z_on / Z_off) = " << efficiency << " promoterOcc(E) = " << promoterOcc << endl;
	}
//#endif
	if ( out_flag == 1 ) {
		cout << Z_on << "\t" << Z_off << "\t" << par.basalTxps [ seq_num ] << endl;
	}
	return promoterOcc;
}

double ExprFunc::predictExpr( const SiteVec& _sites, int length, const vector< double >& factorConcs, int seq_num, int out_flag, double *zOn, double *zOff)
{
    bindingWts.clear();
    boundaries.clear();
	seq_index = seq_num;
	if( !one_qbtm_per_crm )
		seq_num = 0;
// store the sequence
    int n = _sites.size();
    sites = _sites;
    sites.insert( sites.begin(), Site() );  // start with a pseudo-site at position 0
    boundaries.push_back( 0 );
    double range = max( intFunc->getMaxDist(), repressionDistThr );
    for ( int i = 1; i <= n; i++ ) {
        int j;
        for ( j = i - 1; j >= 1; j-- ) {
            if ( ( sites[i].start - sites[j].start ) > range ) break;
        }
        int boundary = j;
        boundaries.push_back( boundary );
    }
// compute the Boltzman weights of binding for all sites
    bindingWts.push_back( 1.0 );
    for ( int i = 1; i <= n; i++ ) {
        bindingWts.push_back( par.maxBindingWts[ sites[i].factorIdx ] * factorConcs[sites[i].factorIdx] * sites[i].wtRatio );
    }
// Logistic model
//    int n_factors = motifs.size();
    if ( modelOption == LOGISTIC ) {
		vector< double > factorOcc( nFactors, 0 ); // total occupancy of each factor
        for ( int i = 1; i < sites.size(); i++ ) {
            factorOcc[ sites[i].factorIdx ] += bindingWts[i] / ( 1.0 + bindingWts[i] );
        }
        double totalEffect = 0;
		for ( int i = 0; i < nFactors; i++ ) {
            double effect = par.txpEffects[i] * factorOcc[i];
            totalEffect += effect;
// length correction
//             totalEffect = totalEffect / (double)length;
        }
//         return par.expRatio * logistic( log( par.basalTxp ) + totalEffect );
//#ifdef ADEBUG
		if ( DEBUG_ON ) {
			cout << "Logistic factor\toccupancy\ttxp_effect" << endl;
			for ( int i = 0; i < nFactors; i++ ) {
				cout << i << "\t" << factorOcc[i] << "\t" << par.txpEffects[i] * factorOcc[i] << endl;
			}
			cout << totalEffect << endl;
		}
//#endif
        return logistic( par.basalTxps[ seq_num ] + totalEffect );
    }
// Thermodynamic models: Direct, Quenching, ChrMod_Unlimited and ChrMod_Limited
// compute the partition functions
	double Z_off = compPartFuncOff();
	double Z_on = compPartFuncOn();
	(*zOff) = Z_off;
	(*zOn) = Z_on;
// compute the expression (promoter occupancy)
	double efficiency = Z_on / Z_off;
	double promoterOcc = efficiency * par.basalTxps[ seq_num ] / ( 1.0 + efficiency * par.basalTxps[ seq_num ] );
	if ( out_flag == 1 ) {
		cout << Z_on << "\t" << Z_off << "\t" << par.basalTxps [ seq_num ] << endl;
	}
	return promoterOcc;
}

double ExprFunc::compPartFuncOff() const
{
    if ( modelOption == CHRMOD_UNLIMITED || modelOption == CHRMOD_LIMITED ) return compPartFuncOffChrMod();
    int n = sites.size() - 1;
    // initialization
    vector< double > Z( n + 1 );
    Z[0] = 1.0;
    vector< double > Zt( n + 1 );
    Zt[0] = 1.0;
    // recurrence
    for ( int i = 1; i <= n; i++ ) {
        double sum = Zt[boundaries[i]];
        for ( int j = boundaries[i] + 1; j < i; j++ ) {
				if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
                sum += compFactorInt( sites[ i ], sites[ j ] ) * Z[ j ];
        }
        Z[i] = bindingWts[ i ] * sum;
        Zt[i] = Z[i] + Zt[i - 1];
    }
    return Zt[n];
}

double ExprFunc::compPartFuncOffChrMod() const
{
    int n = sites.size()- 1;
    // initialization
    vector< double > Z0( n + 1 );
    Z0[0] = 1.0;
    vector< double > Z1( n + 1 );
    Z1[0] = 1.0;
    vector< double > Zt( n + 1 );
    Zt[0] = 1.0;
    // recurrence
    for ( int i = 1; i <= n; i++ ) {
        double sum = Zt[boundaries[i]];
        double sum0 = sum, sum1 = sum;
        for ( int j = boundaries[i] + 1; j < i; j++ ) {
			if ( siteOverlap( sites[ i ], sites[ j ]) ) continue;
            double dist = sites[i].start - sites[j].start;

            // sum for Z0
            sum0 += compFactorInt( sites[i], sites[j] ) * Z0[j];
            if ( dist > repressionDistThr ) sum0 += Z1[j];

            // sum for Z1
            if ( repIndicators[ sites[i].factorIdx ] ) {
                sum1 += compFactorInt( sites[i], sites[j] ) * Z1[j];
                if ( dist > repressionDistThr ) sum1 += Z0[j];
            }
        }
        Z0[i] = bindingWts[i] * sum0;
        if ( repIndicators[ sites[i].factorIdx ] ) Z1[i] = bindingWts[i] * par.repEffects[ sites[i].factorIdx ] * sum1;
        else Z1[i] = 0;
        Zt[i] = Z0[i] + Z1[i] + Zt[i - 1];
    }
    // the partition function
    return Zt[n];
}

double ExprFunc::compPartFuncOn() const
{
    if ( modelOption == DIRECT ) return compPartFuncOnDirect();
    if ( modelOption == QUENCHING ) return compPartFuncOnQuenching();
    if ( modelOption == CHRMOD_UNLIMITED) return compPartFuncOnChrMod_Unlimited();
    if ( modelOption == CHRMOD_LIMITED ) return compPartFuncOnChrMod_Limited();
}

double ExprFunc::compPartFuncOnDirect() const
{
   int n = sites.size() - 1;
//   int n_factors = motifs.size();
    // initialization
    vector< double > Z( n + 1 );
    Z[0] = 1.0;
    vector< double > Zt( n + 1 );
    Zt[0] = 1.0;

    // recurrence
    for ( int i = 1; i <= n; i++ ) {
        double sum = Zt[boundaries[i]];
        for ( int j = boundaries[i] + 1; j < i; j++ ) {
			if ( siteOverlap( sites[ i ], sites[ j ]) ) continue;
            sum += compFactorInt( sites[ i ], sites[ j ] ) * Z[ j ];
        }
        Z[i] = bindingWts[ i ] * par.txpEffects[ sites[i].factorIdx ] * sum;
        Zt[i] = Z[i] + Zt[i - 1];
    }

    return Zt[n];
}

double ExprFunc::compPartFuncOnQuenching() const
{
    int n = sites.size() - 1;
    int N0 = maxContact;
    //vector< vector< double > > Z1( n + 1, N0 + 1 );
    ////vector< vector< double > > Z1( n + 1, vector < double > ( N0 + 1 ) );
    vector < vector < double > >Z1;
    for( int i = 0; i < n + 1; i++ ){
    	Z1.push_back( vector < double > ( N0 + 1, 0 ));
    }
    //vector< vector< double > > Z0( n + 1, N0 + 1 );
    ////vector< vector< double > > Z0( n + 1, vector < double > ( N0 + 1 ) );

	vector < vector <double> > Z0;
	for( int i = 0; i < n + 1; i++ ){
		Z0.push_back( vector < double > ( N0 + 1, 0 ) );
	}
    // k = 0
    for ( int i = 0; i <= n; i++ ) {
        double sum1 = 1, sum0 = 0;
        for ( int j = 1; j < i; j++ ) {
			if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
            bool R = testRepression( sites[j], sites[i] );
            double term = compFactorInt( sites[ i ], sites[ j ] ) * ( Z1[j][0] + Z0[j][0] );
            sum1 += ( 1 - R )* term;
            sum0 += R * term;
        }
        Z1[i][0] = bindingWts[i] * sum1;
        Z0[i][0] = bindingWts[i] * sum0;
    }
//    int n_factors = motifs.size();
    // k >= 1
    for ( int k = 1; k <= N0; k++ ) {
        for ( int i = 0; i <= n; i++ ) {
            if ( i < k ) {
                Z1[i][k] = 0;
                Z1[i][k] = 0;
                continue;
            }
            double sum1 = 0, sum0 = 0;
            for ( int j = 1; j < i; j++ ) {
				if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
                bool R = testRepression( sites[j], sites[i] );
                double effect = actIndicators[sites[j].factorIdx] * ( 1 - testRepression( sites[i], sites[j] ) ) * Z1[j][k - 1] * par.txpEffects[ sites[j].factorIdx];
                double term = compFactorInt( sites[ i ], sites[ j ] ) * ( Z1[j][k] + Z0[j][k] + effect );
                sum1 += ( 1 - R )* term;
                sum0 += R * term;
            }
            Z1[i][k] = bindingWts[i] * sum1;
            Z0[i][k] = bindingWts[i] * sum0;
        }
    }
    double Z_on = 1;
    for ( int i = 1; i <= n; i++ ) {
        for ( int k = 0; k <= N0; k++ ) {
            double term = Z1[i][k] + Z0[i][k];
            Z_on += term;
        }
        for ( int k = 0; k <= N0 - 1; k++ ) {
            Z_on += actIndicators[sites[i].factorIdx] * Z1[i][k] * par.txpEffects[sites[i].factorIdx];
        }
    }
    return Z_on;
}

double ExprFunc::compPartFuncOnChrMod_Unlimited() const
{
    int n = sites.size()- 1;
//    int n_factors = motifs.size();
    // initialization
    vector< double > Z0( n + 1 );
    Z0[0] = 1.0;
    vector< double > Z1( n + 1 );
    Z1[0] = 1.0;
    vector< double > Zt( n + 1 );
    Zt[0] = 1.0;
    // recurrence
    for ( int i = 1; i <= n; i++ ) {
        double sum = Zt[boundaries[i]];
        double sum0 = sum, sum1 = sum;
        for ( int j = boundaries[i] + 1; j < i; j++ ) {
            double dist = sites[i].start - sites[j].start;
			if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
            // sum for Z0
            sum0 += compFactorInt( sites[i], sites[j] ) * Z0[j];
            if ( dist > repressionDistThr ) sum0 += Z1[j];
            // sum for Z1
            if ( repIndicators[ sites[i].factorIdx ] ) {
                sum1 += compFactorInt( sites[i], sites[j] ) * Z1[j];
                if ( dist > repressionDistThr ) sum1 += Z0[j];
            }
        }
        Z0[i] = bindingWts[i] * par.txpEffects[ sites[i].factorIdx ] * sum0;
        if ( repIndicators[ sites[i].factorIdx ] ) Z1[i] = bindingWts[i] * par.repEffects[ sites[i].factorIdx ] * sum1;
        else Z1[i] = 0;
        Zt[i] = Z0[i] + Z1[i] + Zt[i - 1];
    }
    // the partition function
    return Zt[n];
}

double ExprFunc::compPartFuncOnChrMod_Limited() const
{
    int k;
    int n = sites.size()- 1;
//    int n_factors = motifs.size();
    // initialization
    int N0 = maxContact;
    vector < vector < double > > Z0;
    for( int i = 0; i < n + 1; i++ ){
    	Z0.push_back( vector < double >( N0 + 1, 0 ) );
    }
    vector < vector < double > > Z1;
    for( int i = 0; i < n + 1; i++ ){
    	Z1.push_back( vector < double >( N0 + 1, 0 ) );
    }
    vector < vector < double > > Zt;
    for( int i = 0; i < n + 1; i++ ){
    	Zt.push_back( vector < double >( N0 + 1, 0 ) );
    }
    Z0[0][0] = 0;
    Z1[0][0] = 0;
    Zt[0][0] = 1.0;
    for ( int k = 1; k <= N0; k++ ) {
        Z0[0][k] = 0;
        Z1[0][k] = 0;
        Zt[0][k] = 0;
    }
// recurrence
	for ( int i = 1; i <= n; i++ ) {
		double sum0 = Zt[boundaries[i]][0], sum0A = 0, sum1 = sum0;
        for ( int j = boundaries[i] + 1; j < i; j++ ) {
			double dist = sites[i].start - sites[j].start;
			if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
// sum for Z0
			sum0 += compFactorInt( sites[i], sites[j] ) * Z0[j][0];
            if ( dist > repressionDistThr ) {
				sum0 += Z1[j][0];
            }
// sum for Z1
            if ( repIndicators[ sites[i].factorIdx ] ) {
				sum1 += compFactorInt( sites[i], sites[j] ) * Z1[j][0];
                if ( dist > repressionDistThr ) sum1 += Z0[j][0];
            }
        }
        Z0[i][0] = bindingWts[i] * sum0;
#ifdef ADEBUG
        if ( DEBUG_ON ) {
			cout << "OnChrMod_Limited sum0A " << sum0A << " sum0 " << sum0 << " sum1 " << sum1 << endl;
		}
#endif
		if ( repIndicators[ sites[i].factorIdx ] ) {
			double rep = bindingWts[i] * par.repEffects[ sites[i].factorIdx ] * sum1;
			Z1[i][0] = rep;
#ifdef ADEBUG
            if ( DEBUG_ON ) {
				cout << "OnChrMod_Limited rep " << i << " " << sites[i].factorIdx << " " << rep << endl;
			}
#endif
        } else {
			Z1[i][0] = 0;
		}
        Zt[i][0] = Z0[i][0] + Z1[i][0] + Zt[i - 1][0];
    }
	for ( k = 1; k <= N0; k++ ) {
		for ( int i = 1; i <= n; i++ ) {
			double sum0 = Zt[boundaries[i]][k], sum0A = Zt[boundaries[i]][k-1], sum1 = sum0;
			for ( int j = boundaries[i] + 1; j < i; j++ ) {
				double dist = sites[i].start - sites[j].start;
				if ( siteOverlap( sites[ i ], sites[ j ] ) ) continue;
// sum for Z0
				sum0 += compFactorInt( sites[i], sites[j] ) * Z0[j][k];
				sum0A += compFactorInt( sites[i], sites[j] ) * Z0[j][k-1];
				if ( dist > repressionDistThr ) {
					sum0 += Z1[j][k];
					sum0A += Z1[j][k-1];
				}
// sum for Z1
				if ( repIndicators[ sites[i].factorIdx ] ) {
					sum1 += compFactorInt( sites[i], sites[j] ) * Z1[j][k];
					if ( dist > repressionDistThr ) sum1 += Z0[j][k];
				}
			}
			Z0[i][k] = bindingWts[i] * sum0;
#ifdef ADEBUG
			if ( DEBUG_ON ) {
				cout << "OnChrMod_Limited sum0A " << sum0A << " sum0 " << sum0 << " sum1 " << sum1 << endl;
			}
#endif
			if ( actIndicators[ sites[i].factorIdx ] ) {
				double act = bindingWts[i] * par.txpEffects[sites[i].factorIdx] * sum0A;
				Z0[i][k] += act;
#ifdef ADEBUG
				if ( DEBUG_ON ) {
					cout << "OnChrMod_Limited act " << i << " " << sites[i].factorIdx << " " << act << endl;
				}
#endif
			}
			if ( repIndicators[ sites[i].factorIdx ] ) {
				double rep = bindingWts[i] * par.repEffects[ sites[i].factorIdx ] * sum1;
				Z1[i][k] = rep;
#ifdef ADEBUG
				if ( DEBUG_ON ) {
					cout << "OnChrMod_Limited rep " << i << " " << sites[i].factorIdx << " " << rep << endl;
				}
#endif
			} else {
				Z1[i][k] = 0;
			}
			Zt[i][k] = Z0[i][k] + Z1[i][k] + Zt[i - 1][k];
		}
	}
// the partition function
	return sum( Zt[n] );
}

double ExprFunc::compFactorInt( const Site& a, const Site& b ) const
{
    double maxInt = par.factorIntMat( a.factorIdx, b.factorIdx );
    double dist = abs( a.start - b.start );
    bool orientation = ( a.strand == b.strand );
    return intFunc->compFactorInt( maxInt, dist, orientation );
}

bool ExprFunc::testRepression( const Site& a, const Site& b ) const
{
    double dist = abs( a.start - b.start );
    return repressionMat( a.factorIdx, b.factorIdx ) && ( dist <= repressionDistThr );
}

