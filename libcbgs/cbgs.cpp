#include "SeqAnnotator.h"
#include "ExprPar.h"
#include "ExprFunc.h"
#include "cbgs.h"

typedef struct Seq2ExprData {
	vector< SiteVec > seqSites;		// the extracted sites for all sequences
	vector< int > seqLengths;           // lengths of all sequences
	vector< Motif > motifs;		// TF binding motifs
	FactorIntFunc* intFunc;   // function to compute distance-dependent TF-TF interactions
	IntMatrix coopMat;       // cooperativity matrix: C(f,f') = 1 if f and f' bind cooperatively
	vector< bool > actIndicators;   // 1 if the TF is in the activator set
	int maxContact;     // the maximum contact
	vector< bool > repIndicators;    // 1 if the TF is in the repressor set
	IntMatrix repressionMat;    // repression matrix: R(f,f') = 1 if f can repress f'
	double repressionDistThr;   // distance threshold for repression: d_R
	ExprPar params;
	int nFactors;
	int nSeqs;
} Seq2ExprData;

static Seq2ExprData s2edata;

void expression_init(char* _seqFile, double *etf, char* _annFile, char* _motifFile, char* _coopFile, char* _factorInfoFile, char* _repressionFile, char *modelOption, int debug_on)
{
	int rval;
	int print_sites = 0;
	string factor1, factor2;    // buffer for reading factor pairs
	double coopDistThr = 50;
	double factorIntSigma = 50.0;   // sigma parameter for the Gaussian interaction function
	double repressionDistThr = 250;
	int maxContact = 1;
	double gcContent = 0.5;
	double eTF = 0.99;
	FactorIntType intOption = BINARY;     // type of interaction function
	ExprPar::searchOption = CONSTRAINED;      // search option: unconstrained; constrained.
	ExprPar::estBindingOption = 1;
    ExprPar::one_qbtm_per_crm = false;
	ExprFunc::one_qbtm_per_crm = false;
	ExprFunc::DEBUG_ON = false;
	string seqFile(_seqFile), annFile(_annFile), motifFile(_motifFile), coopFile(_coopFile), factorInfoFile(_factorInfoFile), repressionFile(_repressionFile);
// read the sequences
	if ( debug_on == 1 ) {
		ExprFunc::DEBUG_ON = true;
	}
	if (!strcmp(_annFile, "printout")) {
		print_sites = 1;
		annFile.clear();
	}
	vector< Sequence > seqs;
	vector< string > seqNames;
	rval = readSequences( seqFile, seqs, seqNames );
	assert( rval != RET_ERROR );
    s2edata.nSeqs = seqs.size();
// read the motifs
	vector< Motif > motifs;
	vector< string > motifNames;
    vector< double > background = createNtDistr( gcContent );
//initialize the energy threshold factors
	vector < double > energyThrFactors;
// site representation of the sequences
	vector< SiteVec > seqSites( s2edata.nSeqs );
	vector< int > seqLengths( s2edata.nSeqs );
	map< string, int > factorIdxMap;
	if (motifFile.empty()) {
		if ( annFile.empty() ) {        // construct site representation
			cerr << "Neither motif nor ann files are given!" << endl;
			exit( 1 );
		} else {    // read the site representation and compute the energy of sites
			int nFactors;
			rval = readSites( annFile, seqSites, &nFactors );
			assert( rval != RET_ERROR );
			s2edata.nFactors = nFactors + 1;
			energyThrFactors.clear( );
			for ( int index = 0; index < s2edata.nFactors; index++ ){
				energyThrFactors.push_back( eTF );
			}
			for ( int i = 0; i < s2edata.nSeqs; i++ ) {
				seqLengths[i] = seqs[i].size();
			}
		}
	} else {
		rval = readMotifs( motifFile, background, motifs, motifNames );
		assert( rval != RET_ERROR );
		s2edata.nFactors = motifs.size();
		if ( ExprFunc::DEBUG_ON ) {
			for ( int i = 0; i < s2edata.nFactors; i++ ) {
				cout << "Motif number " << i << " length " << motifs[i].length() << endl;
				cout << motifs[i] << endl;
			}
		}
// factor name to index mapping
		for ( int i = 0; i < motifNames.size(); i++ ) {
			factorIdxMap[motifNames[i]] = i;
		}
		if ( annFile.empty() ) {        // construct site representation
			energyThrFactors.clear( );
			for ( int index = 0; index < s2edata.nFactors; index++ ){
				energyThrFactors.push_back( etf[index] );
			}
			SeqAnnotator ann( motifs, energyThrFactors );
			for ( int i = 0; i < s2edata.nSeqs; i++ ) {
				ann.annot( seqs[ i ], seqSites[ i ] );
				seqLengths[i] = seqs[i].size();
			}
		} else {    // read the site representation and compute the energy of sites
			energyThrFactors.clear( );
			for ( int index = 0; index < s2edata.nFactors; index++ ){
				energyThrFactors.push_back( eTF );
			}
			SeqAnnotator ann( motifs, energyThrFactors );
			rval = readSites( annFile, factorIdxMap, seqSites, false );
			assert( rval != RET_ERROR );
			for ( int i = 0; i < s2edata.nSeqs; i++ ) {
				ann.compEnergy( seqs[i], seqSites[i] );
				seqLengths[i] = seqs[i].size();
			}
		}
	}
	if ( ExprFunc::DEBUG_ON || print_sites == 1 ) {
        for ( int i = 0; i < s2edata.nSeqs; i++ ) {
            cout << "Sequence number " << i << " length " << seqSites[i].size() << endl;
			cout << "Site number" << "\t" << "Start + 1" << "\t" << "Strand" << "\t" << "Factor Idx" << "\t" << "Energy" << "\t" << "Wt Ratio" << "\t" << "word" << "\t" << "length" << endl;
            for ( int j = 0; j < seqSites[i].size(); j++ ) {
				cout << j << "\t" << seqSites[i][j] << endl;
            }
        }
    }
// read the cooperativity matrix
	int num_of_coop_pairs = 0;
	IntMatrix coopMat( s2edata.nFactors, s2edata.nFactors, false );
	if ( !coopFile.empty() ) {
		ifstream fcoop( coopFile.c_str() );
		if ( !fcoop ) {
			cerr << "Cannot open the cooperativity file " << coopFile << endl;
			exit( 1 );
		}
		while ( fcoop >> factor1 >> factor2 ) {
			assert( factorIdxMap.count( factor1 ) && factorIdxMap.count( factor2 ) );
			int idx1 = factorIdxMap[factor1];
			int idx2 = factorIdxMap[factor2];
			if( coopMat( idx1, idx2 ) == false && coopMat( idx2, idx1 ) == false ) {
				coopMat( idx1, idx2 ) = true;
				coopMat( idx2, idx1 ) = true;
				num_of_coop_pairs ++;
			}
		}
	}
// read the roles of factors
	vector< bool > actIndicators( s2edata.nFactors, false );
	vector< bool > repIndicators( s2edata.nFactors, false );
	if ( !factorInfoFile.empty() ) {
		ifstream finfo( factorInfoFile.c_str() );
		if ( !finfo ) {
			cerr << "Cannot open the factor information file " << factorInfoFile << endl;
			exit( 1 );
		}
		string name;
		int i = 0, actRole, repRole;
		while ( finfo >> name >> actRole >> repRole ) {
			assert( name == motifNames[i] );
			if ( actRole ) actIndicators[i] = true;
			if ( repRole ) repIndicators[i] = true;
			i++;
		}
	}
// read the repression matrix
	IntMatrix repressionMat( s2edata.nFactors, s2edata.nFactors, false );
	if ( !repressionFile.empty() ) {
		ifstream frepr( repressionFile.c_str() );
		if ( !frepr ) {
			cerr << "Cannot open the repression file " << repressionFile << endl;
			exit( 1 );
		}
		while ( frepr >> factor1 >> factor2 ) {
			assert( factorIdxMap.count( factor1 ) && factorIdxMap.count( factor2 ) );
			int idx1 = factorIdxMap[factor1];
			int idx2 = factorIdxMap[factor2];
			repressionMat( idx1, idx2 ) = true;
		}
	}
	if ( intOption == BINARY ) {
		s2edata.intFunc = new FactorIntFuncBinary( coopDistThr );
	} else if ( intOption == GAUSSIAN ) {
		s2edata.intFunc = new FactorIntFuncGaussian( coopDistThr, factorIntSigma );
	} else {
		cerr << "Interaction Function is invalid " << endl;
		exit( 1 );
	}
	ExprPar::modelOption = getModelOption( modelOption );
	ExprFunc::modelOption = getModelOption( modelOption );
	ExprPar params( s2edata.nFactors, s2edata.nSeqs );
	s2edata.seqSites = seqSites;
	s2edata.seqLengths = seqLengths;
	s2edata.motifs = motifs;
	s2edata.coopMat = coopMat;
	s2edata.actIndicators = actIndicators;
	s2edata.repIndicators = repIndicators;
	s2edata.repressionMat = repressionMat;
	s2edata.params = params;
	s2edata.maxContact = maxContact;
	s2edata.repressionDistThr = repressionDistThr;
}

double expression(double* factors, double* eqparms, int seq_num, double *zOn, double *zOff)
{
	vector< double > concs;
	for ( int i = 0; i < s2edata.nFactors; i++ ) {
		concs.push_back(factors[i]);
	}
	s2edata.params.load( eqparms , seq_num );
	s2edata.maxContact = eqparms[s2edata.nFactors + 2];
	s2edata.repressionDistThr = eqparms[s2edata.nFactors + 1];
	if ( ExprPar::modelOption == CHRMOD_UNLIMITED || ExprPar::modelOption == CHRMOD_LIMITED || ExprPar::modelOption == QUENCHING ) {
		for ( int i = 0; i < s2edata.nFactors; i++ ) {
			if ( eqparms[i] > 0 ) {
				s2edata.actIndicators[i] = true;
				s2edata.repIndicators[i] = false;
				s2edata.repressionMat( i, seq_num ) = false;
			}
			if ( eqparms[i] < 0 ) {
				s2edata.repIndicators[i] = true;
				s2edata.actIndicators[i] = false;
				s2edata.repressionMat( i, seq_num ) = true;
			}
		}
	}
	ExprFunc* func = new ExprFunc( s2edata.intFunc, s2edata.actIndicators, s2edata.maxContact, s2edata.repIndicators, s2edata.repressionMat, s2edata.repressionDistThr, s2edata.params );
	double predicted = func->predictExpr( s2edata.seqSites[seq_num], s2edata.seqLengths[seq_num], concs, seq_num, 0, zOn, zOff );
	delete func;
	return predicted;
}

