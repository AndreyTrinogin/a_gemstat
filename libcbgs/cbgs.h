#ifndef CBGS_H
#define CBGS_H

#ifdef __cplusplus
extern "C" {
#endif
	double expression(double* factors, double* params, int seq_num, double *zOn, double *zOff);
	void expression_init(char* _seqFile, double *etf, char* _annFile, char* _motifFile, char* _coopFile, char* _factorInfoFile, char* _repressionFile, char *modelOption, int debug_on);
#ifdef __cplusplus
}
#endif

#endif
